// create map
const map = L.map("mapid").setView([-21.6955005, -45.2558576], 15);

//create and add tileLayer
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png").addTo(map);

// create icon
const icon = L.icon({
  iconUrl: "/images/map-marker.svg",
  iconSize: [58, 68],
  iconAnchor: [29, 68],
  popupAnchor: [170, 2],
});

let marker;

//create and add marker
map.on("click", (event) => {
  const lat = event.latlng.lat;
  const lng = event.latlng.lng;

  document.querySelector("[name=lat]").value = lat;
  document.querySelector("[name=lng]").value = lng;

  //remove icon
  marker && map.removeLayer(marker);

  //add icon layer
  marker = L.marker([lat, lng], { icon }).addTo(map);
});

//add photos in field
function addPhotoField() {
  // catch the photos container #images
  const container = document.querySelector("#images");
  //catch the container duplicate .new-images
  const fieldsContainer = document.querySelectorAll(".new-upload");
  //does the clone of last image add
  const newFieldContainer = fieldsContainer[
    fieldsContainer.length - 1
  ].cloneNode(true);

  //check if field is null, case yes , no add in container photos
  const input = newFieldContainer.children[0];

  if (input.value == "") {
    return;
  }

  // clean the field before add to container of photos
  input.value = "";

  //add the clone to images container
  container.appendChild(newFieldContainer);
}

function deleteField(event) {
  const span = event.currentTarget;

  const fieldsContainer = document.querySelectorAll(".new-upload");

  if (fieldsContainer.length <= 1)
    //limpar o valor do campo
    span.parentNode.children[0].value = ""
    return;


//deletar o campo
span.parentNode.remove(span);
}

//select yes or no
function toggleSelect(event){
    
    //leave a class .active (buttons)
    document.querySelectorAll('.button-select button')
    .forEach(button => button.classList.remove('active'))
    
    //put a class .active on clicked button

    const button = event.currentTarget
    button.classList.add('active')

    //update the menu input hidden with selected value

    const input = document.querySelector('[name="open_on_weekend"]')

    input.value = button.dataset.value


}